gulp = require('gulp')
gulpif = require('gulp-if')
uglify = require('gulp-uglify')
uglifycss = require('gulp-uglifycss')
less = require('gulp-less')
concat = require('gulp-concat')
sourcemaps = require('gulp-sourcemaps')
shell = require('gulp-shell')
coffee = require('gulp-coffee')
gutil = require('gulp-util')
env = process.env.GULP_ENV

gulp.task 'tpl', ->
  gulp.src([
    'src/GalleryBundle/Resources/assets/js/coffee/**/*.tpl'
  ]).pipe gulp.dest('web/js')

gulp.task 'coffee', ->
  gulp.src([
    'src/GalleryBundle/Resources/assets/js/coffee/**/*.coffee'
  ]).pipe(coffee({bare: true}).on('error', gutil.log)).pipe gulp.dest('web/js')

gulp.task 'js', [ 'coffee' ], ->
  gulp.src([
    'app/Resources/public/assets/js/**/*.js'
    'src/GalleryBundle/Resources/assets/js/**/*.js'
  ]).pipe(concat('scripts.js')).pipe(gulpif(env == 'prod', uglify())).pipe(sourcemaps.write('./')).pipe gulp.dest('web/js')

gulp.task 'copy_js_libs', [ 'coffee' ], ->
  gulp.src([
    'bower_components/jquery/dist/jquery.js'
    'bower_components/backbone/backbone.js'
    'bower_components/bootstrap/dist/js/bootstrap.js'
    'bower_components/backbone.marionette/lib/core/backbone.marionette.js'
    'bower_components/backbone.babysitter/lib/backbone.babysitter.js'
    'bower_components/backbone.wreqr/lib/backbone.wreqr.js'
    'bower_components/requirejs/require.js'
    'bower_components/underscore/underscore.js'
    'bower_components/requirejs-underscore-tpl/underscore-tpl.js'
    'bower_components/text/text.js'
  ]).pipe gulp.dest('web/js/libs')

gulp.task 'css', ->
  gulp.src([
    'bower_components/bootstrap/dist/css/bootstrap.css'
    'src/GalleryBundle/Resources/assets/css/style.css'
    'app/Resources/public/less/**/*.less'
  ]).pipe(gulpif(/[.]less/, less())).pipe(concat('styles.css')).pipe(gulpif(env == 'prod', uglifycss())).pipe(sourcemaps.write('./')).pipe gulp.dest('web/css')

gulp.task 'img', ->
  gulp.src('app/Resources/public/img/**/*.*').pipe gulp.dest('web/img')

gulp.task 'console', shell.task([
  'composer install'
  'bower install'
  'php app/console doctrine:database:create --if-not-exists'
  'php app/console doctrine:schema:update --force'
])

gulp.task 'clear_cache', shell.task([
  'rm -rf ./web/js'
])

gulp.task 'initial', [ 'console' ], ->
  gulp.start 'dev'
  return

gulp.task 'dev', [ 'clear_cache' ], ->
  gulp.start 'default'
  return

gulp.task 'default', [
  'copy_js_libs',
  'js'
  'css'
  'img'
  'tpl'
]