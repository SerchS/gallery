<?php

namespace GalleryApiBundle\Controller;

use GalleryApiBundle\Services\AlbumManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use JMS\Serializer\SerializationContext;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AlbumController
 *
 * @Route("/album", defaults={"_format" = "json"})
 * @package GalleryApiBundle\Controller
 */
class AlbumController extends Controller
{
    /**
     * Get album list
     *
     * @Get("/list", name="album_list")
     * @View()
     *
     * @return array
     */
    public function listAction()
    {
        return $this->get('album.manager')->getList();
    }

    /**
     * Get album info
     *
     * @param $id
     *
     * @Get(
     *     "/get/{id}/{page}",
     *     defaults={"page" = 1},
     *     requirements={"id" = "\d+", "page" = "\d+"},
     *     name="album_info"
     * )
     * @View()
     *
     * @return array
     */
    public function getAction($id, $page)
    {
        return $this->get('image.manager.paginator')->getList($id, $page);
    }
}
