<?php

namespace GalleryApiBundle\Services;

use Doctrine\ORM\EntityManager;
use GalleryBundle\Entity\Image;
use Knp\Component\Pager\Pagination\PaginationInterface;

class ImageManager {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \GalleryBundle\Repository\ImageRepository
     */
    protected $imageRepository;

    /**
     * @var int
     */
    private $itemsPerPage;
    
    /**
     * @var \Knp\Component\Pager\Paginator
     */
    private $paginator;

    /**
     * AlbumManager constructor.
     *
     * @param EntityManager $entityManager
     * @param \Knp\Component\Pager\Paginator $paginator
     * @param int $itemsPerPage
     */
    public function __construct
    (
        EntityManager $entityManager,
        $paginator,
        $itemsPerPage
    )
    {
        $this->em = $entityManager;
        $this->setPaginator($paginator);
        $this->setItemsPerPage($itemsPerPage);
        $this->imageRepository = $this->em->getRepository('GalleryBundle:Image');
    }

    /**
     * @param \Knp\Component\Pager\Paginator $paginator
     *
     * @return $this
     */
    public function setPaginator($paginator)
    {
        $this->paginator = $paginator;
        return $this;
    }

    /**
     * @return \Knp\Component\Pager\Paginator
     */
    public function getPaginator()
    {
        return $this->paginator;
    }

    /**
     * Set count items per page
     *
     * @param int $itemsPerPage
     *
     * @return $this
     */
    public function setItemsPerPage($itemsPerPage)
    {
        $this->itemsPerPage = (int)$itemsPerPage;

        return $this;
    }

    /**
     * Get count items per page
     *
     * @return int
     */
    public function getItemsPerPage()
    {
        return $this->itemsPerPage;
    }

    /**
     * Get list of album
     *
     * @param int $albumId
     * @param int $page
     *
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function getList($albumId, $page)
    {
        return $this->getPaginator()->paginate(
            $this->imageRepository->getList($albumId),
            $page,
            10
        );
    }

}