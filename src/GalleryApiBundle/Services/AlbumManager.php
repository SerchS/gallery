<?php

namespace GalleryApiBundle\Services;

use Doctrine\ORM\EntityManager;
use GalleryBundle\Entity\Album;

class AlbumManager {

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \GalleryBundle\Repository\AlbumRepository
     */
    protected $albumRepository;

    /**
     * @var int
     */
    private $minCountImages;

    /**
     * AlbumManager constructor.
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->albumRepository = $this->em->getRepository('GalleryBundle:Album');
    }

    /**
     * Set minimal count images
     *
     * @param int $minCountImages
     *
     * @return $this
     */
    public function setMinCountImages($minCountImages)
    {
        $this->minCountImages = (int)$minCountImages;

        return $this;
    }

    /**
     * Get minimal count images
     *
     * @return int
     */
    public function getMinCountImages()
    {
        return $this->minCountImages;
    }

    /**
     * Get list of album
     *
     * @return array
     */
    public function getList()
    {
        return $this->albumRepository->getList($this->getMinCountImages())->getQuery()->getArrayResult();
    }

    /**
     * Get album info
     * 
     * @param $id
     *
     * @return null|array
     */
    public function find($id)
    {
        /** @var Album $album */
        if ($album = $this->albumRepository->find($id)) {
            $album = $album->toArray();
        }
        return $album;
    }
}