define [
  "marionette"
  "jquery"
], (Marionette) ->

  GalleryApp = new Marionette.Application(
    onStart: ->
      if Backbone.history
        Backbone.history.start()
        if this.checkRoute() == ''
          GalleryApp.trigger 'album:list'
        else if this.checkRoute() != 'albums'
          GalleryApp.trigger 'load:layout'

     checkRoute: ->
       Backbone.history.fragment

    navigate: (route, options = {}) ->
      Backbone.history.navigate route, options

    startSubApp: (subApp) ->
      currentApp = GalleryApp.module(subApp)
      GalleryApp.currentApp = currentApp
      currentApp.start()
  )

  RegionContainer = Marionette.LayoutView.extend(
    el: '#main-block'
    regions:
      albumList: '#album-list-block'
      imageList: '#image-list-block'
  )
  GalleryApp.regions = new RegionContainer

  GalleryApp