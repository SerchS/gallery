define [
  'main',
  'tpl!modules/common/views/templates/paginator.tpl'
], (GalleryApp, paginatorTemp) ->
  GalleryApp.module 'Common.PaginatorView', (View, GalleryApp, Backbone, Marionette, $, _) ->
    View.ItemView = Marionette.ItemView.extend(
      tagName: 'div'
      template: paginatorTemp
    )

    View.Controller = Marionette.Controller.extend(
      show: (options) ->
        @itemView = @getView(options)
        options.region.show @itemView

      getView: (options)->
        itemView = new View.ItemView(options)
        itemView
    )

    View.addInitializer ->
      View.itemView = new View.ItemView
      View.controller = new View.Controller

  GalleryApp.Common.PaginatorView