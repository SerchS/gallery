<% var paginationItemCount = Math.ceil(total_count/num_items_per_page) %>
<% if (paginationItemCount > 1) { %>
<nav>
    <ul class="pagination">
        <% if (typeof current_page_number != 'undefined' && current_page_number > 1) { %>
        <li>
            <a href="#album/<%- params.id %>" aria-label="Previous">
                <span aria-hidden="false">&laquo;</span>
            </a>
        </li>
        <% } %>
        <% for(var i = 1; i <= paginationItemCount; i++) { %>
        <li class="<% if ((typeof current_page_number != 'undefined' && current_page_number == i)
                || (typeof current_page_number == 'undefined' && i == 1)) { %>
            disabled
            <% } else { %>navigate-to<% } %>"><a href="#album/<%- params.id %>/page/<%-i %>"><%-i %></a></li>
        <% } %>
        <% if ((typeof current_page_number != 'undefined' && (paginationItemCount - current_page_number) >= 1) || (typeof current_page_number == 'undefined')) { %>
        <li>
            <a href="#album/<%- params.id %>/page/<%- paginationItemCount %>" class="next" aria-label="Next">
                <span aria-hidden="false">&raquo;</span>
            </a>
        </li>
        <% } %>
    </ul>
</nav>
<% } %>