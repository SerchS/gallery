define [
  'main',
], (GalleryApp) ->
  GalleryApp.module 'Albums', (Albums, GalleryApp, Backbone, Marionette, $, _) ->
    Albums.Router = Marionette.AppRouter.extend(
      appRoutes:
        'albums': 'AlbumList'
        'album/:id': 'AlbumItem'
        'album/:id/page/:page': 'AlbumItem'
    )

    Albums.Controller = Marionette.Controller.extend(
      AlbumList: ->
        GalleryApp.startSubApp('Albums.Manager')
        collection = Albums.Manager.controller.list()
        GalleryApp.startSubApp('Albums.ListView')
        Albums.ListView.controller.show(
          region: GalleryApp.regions.albumList
          collection: collection
        )
      AlbumItem: (id, page)->
        GalleryApp.startSubApp('Albums.Manager')
        response = Albums.Manager.controller.item(id, page)

        $.when (response).done (result) ->
          GalleryApp.startSubApp('Images.ListView')
          collection = new Backbone.Collection(result.get('items'))
          GalleryApp.startSubApp('Common.PaginatorView')

          layoutView = GalleryApp.Images.ListView.controller.getLayoutView()

          GalleryApp.regions.imageList.show(layoutView)

          GalleryApp.Images.ListView.controller.showImageList(
            region: layoutView.getRegion('imageBlock')
            collection: collection
          )
          
          layoutView.getRegion('paginator').show GalleryApp.Common.PaginatorView.controller.getView(model: result)

    )

    Albums.addInitializer ->
      Albums.controller = new Albums.Controller
      Albums.router = new Albums.Router(
        controller: Albums.controller
      )

    GalleryApp.on 'load:layout', ->
      Albums.controller.AlbumList()

    GalleryApp.on 'album:list', ->
      GalleryApp.navigate 'albums'
      Albums.controller.AlbumList()

    GalleryApp.on 'album:item', (id, page) ->
      if (page)
        GalleryApp.navigate 'album/'+id+'/'+page
      else
        GalleryApp.navigate 'album/'+id
      Albums.controller.AlbumItem(id, page)

  GalleryApp.Albums
