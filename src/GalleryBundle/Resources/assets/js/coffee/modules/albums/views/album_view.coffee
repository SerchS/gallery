define [
  'main',
  'tpl!modules/albums/views/templates/item.tpl'
  'tpl!modules/albums/views/templates/item_list.tpl'
], (GalleryApp, itemTemp, listTemp) ->
  GalleryApp.module 'Albums.ListView', (ListView, GalleryApp, Backbone, Marionette, $, _) ->
    ListView.View = Marionette.ItemView.extend(
      tagName: 'li'
      template: itemTemp
      triggers:
        'click a item_album': 'album:show'
    )

    ListView.CompositeView = Marionette.CompositeView.extend(
      template: listTemp
      childView: ListView.View
      childViewContainer: 'ul'
    )

    ListView.Controller = Marionette.Controller.extend(
      show: (options) ->
        @compositeView = @getCompositeView(options)
        options.region.show @compositeView

      getCompositeView: (options)->
        compositeView = new ListView.CompositeView(options)
        compositeView
    )

    ListView.addInitializer ->
      ListView.itemView = new ListView.View
      ListView.compositeView = new ListView.CompositeView
      ListView.controller = new ListView.Controller

  GalleryApp.Albums.List