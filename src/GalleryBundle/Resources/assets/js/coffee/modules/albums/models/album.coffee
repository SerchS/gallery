define [
  'main'
], (GalleryApp) ->
  GalleryApp.module 'Albums.Manager', (Manager, GalleryApp, Backbone, Marionette, $, _) ->
    Manager.Item = Backbone.Model.extend(
      url: ->
        url = 'api/album/get/'+this.get 'id'
        if this.get 'page'
          url += '/' + this.get 'page'
        url
    )

    Manager.Collection = Backbone.Collection.extend(
      url: 'api/album/list'
    )

    Manager.Controller = Marionette.Controller.extend(
      list: ->
        item = new Manager.Collection
        item.fetch({})
        item.forEach (item) ->
          item.save()
        item

      item: (id, page) ->
        item = new Manager.Item({
          id: id
          page: page
        })
        defer = $.Deferred()
        setTimeout  (->
          item.fetch(
            success: (data) ->
              defer.resolve data

            error: (data) ->
              defer.resolve undefined
          )
        ), 300
        defer.promise()
    )

    Manager.addInitializer ->
      Manager.item = new Manager.Item
      Manager.collection = new Manager.Collection
      Manager.controller = new Manager.Controller
