define [
  'main',
  'tpl!modules/images/views/templates/item.tpl'
  'tpl!modules/images/views/templates/item_list.tpl'
  'tpl!modules/images/views/templates/layout_item_list.tpl'
], (GalleryApp, itemTemp, listTemp, layoutItemListTemp) ->
  GalleryApp.module 'Images.ListView', (ListView, GalleryApp, Backbone, Marionette, $, _) ->
    ListView.View = Marionette.ItemView.extend(
      tagName: 'div'
      template: itemTemp
      className: 'col-lg-3 col-md-4 col-xs-6 thumb'
    )

    ListView.CompositeView = Marionette.CompositeView.extend(
      childViewContainer: "#image_list_container"
      template: listTemp
      childView: ListView.View
    )

    ListView.LayoutView = Marionette.LayoutView.extend(
      template: layoutItemListTemp
      regions:
        imageBlock: '#images-block'
        paginator: '#paginator-block'
    )

    ListView.Controller = Marionette.Controller.extend(
      showImageList: (options) ->
        @compositeView = @getCompositeView(options)
        options.region.show @compositeView

      getCompositeView: (options)->
        compositeView = new ListView.CompositeView(options)
        compositeView

      getLayoutView: (options) ->
        layoutView = new ListView.LayoutView(options)
        layoutView
    )

    ListView.addInitializer ->
      ListView.itemView = new ListView.View
      ListView.compositeView = new ListView.CompositeView
      ListView.controller = new ListView.Controller
      ListView.layoutView = new ListView.LayoutView

  GalleryApp.Images.List