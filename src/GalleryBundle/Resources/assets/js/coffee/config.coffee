requirejs.config
  baseUrl: "/js"
  waitSeconds: 15
  paths:
    backbone: "libs/backbone"
    marionette: "libs/backbone.marionette"
    'backbone.babysitter': 'libs/backbone.babysitter'
    'backbone.wreqr': 'libs/backbone.wreqr'
    jquery: "libs/jquery"
    text: "libs/text"
    underscore: "libs/underscore"
    tpl: "libs/underscore-tpl"

require [
  "main"
  "modules/albums/gallery_router"
  "modules/albums/views/album_view"
  "modules/albums/models/album"
  "modules/images/views/image_view"
  "modules/common/views/paginator_view"
], (GalleryApp) ->
  GalleryApp.start()