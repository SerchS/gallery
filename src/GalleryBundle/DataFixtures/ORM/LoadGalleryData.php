<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use GalleryBundle\Entity\Album;
use GalleryBundle\Entity\Image;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $countImage = $i?rand(20, 100):5;
            $album = new Album();
            $album->setName("Album ".($i+1));
            $manager->persist($album);
            for ($j = 0; $j < $countImage; $j++) {
                $img = new Image();
                $img->setFileName('image.jpg');
                $img->setAlbum($album);
                $manager->persist($img);
            }
        }
        $manager->flush();
    }
}